import React, { useCallback, Fragment } from 'react';
import PropTypes from 'prop-types';
import './result-container.scss';

import Form from '../../components/Form/Form'
import Wrapper from '../../components/Wrapper/Wrapper'
import FormHeader from '../../components/CustomForm/FormHeader/FormHeader';
import FormCancel from '../../components/CustomForm/FormCancel/FormCancel';
import FormSubmit from '../../components/CustomForm/FormSubmit/FormSubmit';
import FormSeparator from '../../components/CustomForm/FormSeperator/FormSeparator';
import FormField from '../../components/CustomForm/FormField/FormField';

function ResultConfigurator({ structure, onCancel, onSubmit }) {
  const { header, content, footer } = structure;

  const onSubmitHandler = useCallback((event) => {
    event.preventDefault();
    onSubmit(event);
  }, [onSubmit]);

  return (
    <Form
      className='result-container__form'
      name='result'
      onSubmit={onSubmitHandler}
    >
      {header && (
        <Fragment>
          <Wrapper className='result-container__header'>
            <FormHeader {...header}/>
          </Wrapper>

          <FormSeparator className='result-container__separator'/>
        </Fragment>
      )}

      {content && (
        <Wrapper className='result-container__content'>
          {content.items.map((item, index) => (
            <FormField
              key={`${item.type}--${index}`}
              id={`form-field--${index}`}
              name={`form-field--${index}`}
              {...item}
            />
          ))}
        </Wrapper>
      )}

      {footer && (
        <Fragment>
          <FormSeparator className='result-container__separator'/>

          <Wrapper className='result-container__footer'>
            {footer.cancel && (
              <FormCancel
                label={footer.cancel.label}
                onClick={onCancel}
              />
            )}
            {footer.submit && (
              <FormSubmit
                label={footer.submit.label}
              />
            )}
          </Wrapper>
        </Fragment>
      )}
    </Form>
  );
}

ResultConfigurator.propTypes = {
  structure: PropTypes.shape({
    header: PropTypes.object,
    content: PropTypes.object,
    footer: PropTypes.object,
  }),
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
};

export default ResultConfigurator;
