import React, { useCallback, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import './config-container.scss';

import Form from '../../components/Form/Form'
import Wrapper from '../../components/Wrapper/Wrapper'
import TextArea from '../../components/TextArea/TextArea'
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage'
import SubmitButton from '../../components/SubmitButton/SubmitButton'
import { validateFormStructure } from '../../services/utils/utils';
import UploadButton from '../../components/UploadMock/UploadButton';

function ConfigContainer({ onSubmit }) {
  const inputRef = useRef(false);
  const [errorMessage, setErrorMessage] = useState(false);

  const onChangeInput = useCallback(() => {
    if (errorMessage) {
      setErrorMessage(false);
    }
  }, [errorMessage, setErrorMessage]);

  const onUploadClick = useCallback(() => {
    try {
      inputRef.current.value = JSON.stringify(require('../../mock/mock.json'));
      setErrorMessage(false)
    } catch (error) {
      setErrorMessage(error.message)
    }
  }, [inputRef])

  const onSubmitForm = useCallback((event) => {
    event.preventDefault();

    const [value, error] = validateFormStructure(inputRef.current.value);

    if (error) {
      setErrorMessage(error);
      inputRef.current.focus();
      return;
    }

    onSubmit(value);

  }, [inputRef, onSubmit]);

  return (
    <Form
      className='config-container__form'
      name='config'
      onSubmit={onSubmitForm}
    >
      <Wrapper className='config-container__content'>
        <TextArea
          className='config-container__textarea'
          storeRef={inputRef}
          onChange={onChangeInput}
        />
      </Wrapper>
      <Wrapper className='config-container__footer'>
        <ErrorMessage
          className='config-container__error'
          message={errorMessage}
        />
        <UploadButton
          className='config-container__upload'
          label='Upload mock'
          onClick={onUploadClick}
        />
        <SubmitButton
          className='config-container__submit'
          disabled={!!errorMessage}
          label='Submit'
        />
      </Wrapper>
    </Form>
  );
}

ConfigContainer.propTypes = {
  onSubmit: PropTypes.func.isRequired,
}

export default ConfigContainer;
