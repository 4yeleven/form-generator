import React, { useCallback, useState } from 'react';
import './application.scss';

import Workspace from '../components/Workspace/Workspace';
import TabPanel from '../components/TabPanel/TabPanel';
import TabPanelHeader from '../components/TabPanel/TabPanelHeader';
import TabPanelContent from '../components/TabPanel/TabPanelContent';
import TabPanelButton from '../components/TabPanel/TabPanelButton';

import ConfigContainer from '../containers/ConfigContainer/ConfigContainer';
import ResultConfigurator from '../containers/ResultContainer/ResultContainer';
import { download } from '../services/utils/utils';

const tabTypes = {
  config: 'config',
  result: 'result',
};

function Application() {
  const [activeTab, setActiveTab] = useState(tabTypes.config);
  const [formStructure, setFormStructure] = useState(false);

  const onClickTabButton = useCallback((tabType) => {
    return () => {
      setActiveTab(tabType);
    };
  }, [setActiveTab]);

  const onSubmitConfigForm = useCallback((formStructure) => {
    setFormStructure(formStructure);
    setActiveTab(tabTypes.result);
  }, [setActiveTab])

  const onSubmitResultForm = useCallback((s) => {
    download('form.json', JSON.stringify(formStructure))
  }, [formStructure]);

  const onCancelResultForm = useCallback(() => {
    setActiveTab(tabTypes.config);
  }, [setActiveTab]);

  const isActiveConfig = (activeTab === tabTypes.config);
  const isActiveResult = !! ((activeTab === tabTypes.result) && formStructure);

  return (
    <Workspace>
      <TabPanel>
        <TabPanelHeader>
          <TabPanelButton
            label='Config'
            isActive={isActiveConfig}
            onClick={onClickTabButton(tabTypes.config)}
          />
          <TabPanelButton
            label='Result'
            isActive={isActiveResult}
            isDisabled={!formStructure}
            onClick={onClickTabButton(tabTypes.result)}
          />
        </TabPanelHeader>
        <TabPanelContent>
          {isActiveConfig && (
            <ConfigContainer
              onSubmit={onSubmitConfigForm}
            />
          )}
          {isActiveResult && (
            <ResultConfigurator
              structure={formStructure}
              onCancel={onCancelResultForm}
              onSubmit={onSubmitResultForm}
            />
          )}
        </TabPanelContent>
      </TabPanel>
    </Workspace>
  );
}

export default Application;
