import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './submit-button.scss';

function SubmitButton({ className, disabled, label }) {
  return (
    <button
      className={classnames(className, 'submit-button')}
      disabled={disabled}
      type='submit'
    >
      {label}
    </button>
  );
}

SubmitButton.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.string,
};

export default SubmitButton;
