import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './form.scss';

function Form({ className, name, onSubmit, children }) {
  return (
    <form
      className={classnames('form', className)}
      name={name}
      onSubmit={onSubmit}
    >
      {children}
    </form>
  );
}

Form.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
  onSubmit: PropTypes.func,
  children: PropTypes.any,
};

export default Form;
