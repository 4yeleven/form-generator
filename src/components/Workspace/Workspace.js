import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './workspace.scss';

function Workspace({ className, children }) {
  return (
    <div className={classnames(className, 'workspace')}>
      {children}
    </div>
  );
}

Workspace.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

export default Workspace;
