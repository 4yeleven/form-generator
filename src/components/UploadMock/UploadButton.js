import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './upload-button.scss';

function UploadButton({ className, label, onClick }) {
  return (
    <button
      className={classnames(className, 'upload-button')}
      type='button'
      onClick={onClick}
    >
      {label}
    </button>
  );
}

UploadButton.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
};

export default UploadButton;
