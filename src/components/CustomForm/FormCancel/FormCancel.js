import React from 'react';
import PropTypes from 'prop-types';
import './form-cancel.scss';

function FormCancel({ label, onClick }) {
  return (
    <button
      className='form-cancel'
      type='button'
      onClick={onClick}
    >
      {label}
    </button>
  );
}

FormCancel.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func,
};

export default FormCancel;
