import React from 'react';
import PropTypes from 'prop-types';
import './form-header.scss';

function FormHeader({ label }) {
  return (
    <h3>
      {label}
    </h3>
  )
}

FormHeader.propTypes = {
  label: PropTypes.string,
}

export default FormHeader;
