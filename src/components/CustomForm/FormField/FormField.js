import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './form-field.scss';

import Wrapper from '../../Wrapper/Wrapper';
import Label from './components/Label';
import Text from './components/Text';
import Number from './components/Number';
import Textarea from './components/Textarea';
import Checkbox from './components/Checkbox';
import Date from './components/Date';
import Select from './components/Select';
import Radio from './components/Radio';

import { formFieldTypes } from '../../../services/constants/constants'


const formFieldComponentMap = {
  [formFieldTypes.text]: Text,
  [formFieldTypes.number]: Number,
  [formFieldTypes.textarea]: Textarea,
  [formFieldTypes.checkbox]: Checkbox,
  [formFieldTypes.date]: Date,
  [formFieldTypes.select]: Select,
  [formFieldTypes.radio]: Radio,
};

function FormField({ className, id, type, label, value, placeholder, ...rest }) {
  const FormFieldComponent = formFieldComponentMap[type];

  return (
    <Wrapper className={classnames(className, 'form-field__wrapper')}>
      <Wrapper className='form-field__col'>
        <Label
          label={label}
          htmlFor={id}
        />
      </Wrapper>
      <Wrapper className='form-field__col'>
        <FormFieldComponent
          className='form-field__value'
          id={id}
          value={value}
          placeholder={placeholder}
          {...rest}
        />
      </Wrapper>
    </Wrapper>
  );
};

FormField.propTypes = {
  className: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  type: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
    PropTypes.array,
  ]),
  items: PropTypes.arrayOf(PropTypes.object),
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

export default FormField;
