import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

function Select({ className, id, name, value: selectValue, multiple, items, ...rest }) {
  return (
    <select
      className={classnames(className, 'form-field__select')}
      id={id}
      name={name}
      defaultValue={selectValue}
      multiple={multiple}
      {...rest}
    >
      {items.map(({ label, value }, index) => (
        <option
          key={`${value}-${index}`}
          value={value}
        >
          {label}
        </option>
      ))}
    </select>
  );
}

Select.propTypes = {
  className: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  name: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.array,
  ]),
  multiple: PropTypes.bool,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
    })
  ),
};

Select.defaultProps = {
  multiple: false,
}

export default Select;
