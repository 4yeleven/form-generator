import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

function Text({ className, id, name, value, placeholder, ...rest }) {
  return (
    <input
      className={classnames(className, 'form-field__text')}
      id={id}
      name={name}
      type='text'
      defaultValue={value}
      placeholder={placeholder}
      {...rest}
    />
  );
}

Text.propTypes = {
  className: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  name: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

export default Text;
