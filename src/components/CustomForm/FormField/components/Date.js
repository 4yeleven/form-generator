import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

function Date({ className, id, name, value, placeholder, ...rest }) {
  return (
    <input
      className={classnames(className, 'form-field__date')}
      id={id}
      name={name}
      type='date'
      defaultValue={value}
      placeholder={placeholder}
      {...rest}
    />
  );
}

Date.propTypes = {
  className: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  name: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

export default Date;
