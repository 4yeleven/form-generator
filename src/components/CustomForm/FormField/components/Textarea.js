import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

function Textarea({ className, id, name, value, placeholder, ...rest }) {
  return (
    <textarea
      className={classnames(className, 'form-field__textarea')}
      id={id}
      name={name}
      defaultValue={value}
      placeholder={placeholder}
      {...rest}
    />
  );
}

Textarea.propTypes = {
  className: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  name: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

export default Textarea;
