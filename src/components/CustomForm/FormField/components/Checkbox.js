import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

function Checkbox({ className, id, name, value, ...rest }) {
  return (
    <input
      className={classnames(className, 'form-field__checkbox')}
      id={id}
      name={name}
      type='checkbox'
      defaultChecked={value}
      {...rest}
    />
  );
}

Checkbox.propTypes = {
  className: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  name: PropTypes.string,
  value: PropTypes.bool,
};

export default Checkbox;
