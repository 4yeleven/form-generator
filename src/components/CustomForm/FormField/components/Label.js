import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

function Label({ className, htmlFor, label }) {
  return (
    <label
      className={classNames(className, 'form-field__label')}
      htmlFor={htmlFor}
    >
      {label}
    </label>
  );
}

Label.propTypes = {
  className: PropTypes.string,
  htmlFor: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
}

export default Label;