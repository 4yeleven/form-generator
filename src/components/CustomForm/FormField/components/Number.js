import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

function Number({ className, id, name, value, placeholder, ...rest }) {
  return (
    <input
      className={classnames(className, 'form-field__number')}
      id={id}
      name={name}
      type='number'
      defaultValue={value}
      placeholder={placeholder}
      {...rest}
    />
  );
}

Number.propTypes = {
  className: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  name: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

export default Number;
