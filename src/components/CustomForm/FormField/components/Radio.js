import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

function Radio({ className, id, name, value: radioValue, items, ...rest }) {
  return (
    <div className={classnames(className, 'form-field__radio')}>
      {items.map(({ label, value }, index) => (
        <div
          className='form-field__radio_wrapper'
          key={`${value}--${index}`}
        >
          <input
            id={`${value}--${index}`}
            name={name}
            className={classnames(className, 'form-field__radio_value')}
            type='radio'
            defaultChecked={value === radioValue}
            {...rest}
          />
          <label
            htmlFor={`${value}--${index}`}
            className={classnames(className, 'form-field__radio_label')}
          >
            {label}
          </label>
        </div>
      ))}
    </div>
  );
}

Radio.propTypes = {
  className: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  name: PropTypes.string,
  value: PropTypes.string,
};

export default Radio;
