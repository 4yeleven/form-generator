import React from 'react';
import PropTypes from 'prop-types';
import './form-submit.scss';

function FormSubmit({ label, onClick }) {
  return (
    <button
      className='form-submit'
      type='submit'
      onClick={onClick}
    >
      {label}
    </button>
  );
}

FormSubmit.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func,
};

export default FormSubmit;
