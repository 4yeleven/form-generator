import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './form-separator.scss';

function FormSeparator({ className }) {
  return (<div className={classnames(className, 'form-separator')}/>)
}

FormSeparator.propTypes = {
  className: PropTypes.string,
};

export default FormSeparator;
