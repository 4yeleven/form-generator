import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

function ErrorMessage({ className, message }) {
  return (
    <span className={classnames(className, 'error-message')}>
      {message}
    </span>
  );
}

ErrorMessage.propTypes = {
  className: PropTypes.string,
  message: PropTypes.string,
}

export default ErrorMessage;
