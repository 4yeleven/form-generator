import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './wrapper.scss';

function Wrapper({ className, children }) {
  return (
    <div className={classnames(className, 'wrapper')}>
      {children}
    </div>
  )
}

Wrapper.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

export default Wrapper;
