import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './tab-panel.scss';

function TabPanelContent({ className, children }) {
  return (
    <div className={classnames(className, 'tab-panel__content')}>
      {children}
    </div>
  );
}

TabPanelContent.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};


export default TabPanelContent;