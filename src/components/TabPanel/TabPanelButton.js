import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './tab-panel.scss';

function TabPanelButton({ className, label, isActive, isDisabled, onClick }) {
  return (
    <button
      className={classnames(
        className,
        'tab-panel__button', {
          'tab-panel__button--active': isActive,
        }
      )}
      type='button'
      disabled={isDisabled}
      onClick={onClick}
    >
      {label}
    </button>
  );
}

TabPanelButton.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  isActive: PropTypes.bool,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};


export default TabPanelButton;