import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './tab-panel.scss';

function TabPanelFooter({ className, valign, children }) {
  return (
    <div className={classnames(
      className,
      'tab-panel__footer', {
        [`tab-panel__footer--valign-${valign}`]: valign
      }
    )}>
      {children}
    </div>
  );
}

TabPanelFooter.propTypes = {
  className: PropTypes.string,
  valign: PropTypes.oneOf([
    'start',
    'center',
    'end',
  ]),
  children: PropTypes.any,
};

TabPanelFooter.defaultProps = {
  valign: 'start',
}

export default TabPanelFooter;