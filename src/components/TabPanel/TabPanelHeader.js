import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './tab-panel.scss';

function TabPanelHeader({ className, children }) {
  return (
    <div className={classnames(className, 'tab-panel__header')}>
      {children}
    </div>
  );
}

TabPanelHeader.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};


export default TabPanelHeader;