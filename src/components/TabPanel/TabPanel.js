import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './tab-panel.scss';

function TabPanel({ className, children }) {
  return (
    <div className={classnames(className, 'tab-panel')}>
      {children}
    </div>
  );
}

TabPanel.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};


export default TabPanel;
