import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './textarea.scss';

function TextArea({ className, storeRef, onChange, onKeyDown }) {
  return (
    <textarea
      ref={storeRef}
      className={classnames(className, 'textarea')}
      onChange={onChange}
      onKeyDown={onKeyDown}
    />
  )
}

TextArea.propTypes = {
  storeRef: PropTypes.object,
  className: PropTypes.string,
  onChange: PropTypes.func,
  onKeyDown: PropTypes.func,
};

export default TextArea;
