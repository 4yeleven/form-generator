export default function validateAndPrefill(field) {
  let filledField = {}, errorMessage;

  filledField.type = field.type;
  filledField.label = field.label;
  
  if ('value' in field) {
    if (typeof field.value === 'string' || typeof field.value === 'number') {
      filledField.value = field.value;
    } else {
      errorMessage = 'Unexpected value in text field'

      return [null, errorMessage];
    }
  }

  if ('placeholder' in field) {
    if (typeof field.placeholder === 'string' || typeof field.placeholder === 'number') {
      filledField.placeholder = field.placeholder;
    } else {
      errorMessage = 'Unexpected placeholder in text field'

      return [null, errorMessage];
    }
  }

  return [filledField, errorMessage];
}
