export default function validateAndPrefill(field) {
  let filledField = {}, errorMessage;

  filledField.type = field.type;
  filledField.label = field.label;
  
  if ('value' in field) {
    if (typeof field.value === 'string') {
      filledField.value = field.value;
    } else {
      errorMessage = 'Unexpected value in textarea field'

      return [null, errorMessage];
    }
  }

  if ('placeholder' in field) {
    if (typeof field.placeholder === 'string') {
      filledField.placeholder = field.placeholder;
    } else {
      errorMessage = 'Unexpected placeholder in textarea field'

      return [null, errorMessage];
    }
  }

  return [filledField, errorMessage];
}
