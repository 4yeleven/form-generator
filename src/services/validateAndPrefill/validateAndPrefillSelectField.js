export default function validateAndPrefill(field) {
  let filledField = {}, errorMessage, fieldItemsValues = [];

  filledField.type = field.type;
  filledField.label = field.label;
  filledField.multiple = !!(field.multiple);

  if ('multiple' in field) {
    if (typeof field.multiple === 'boolean') {
      filledField.multiple = field.multiple;
    } else {
      errorMessage = 'Unexpected type of select multiple: expected boolean';
      return [null, errorMessage];
    }
  }

  if ('items' in field && Array.isArray(field.items)) {
    filledField.items = [];
    for (let i = 0; i < field.items.length; i++) {
      const item = field.items[i];
      filledField.items[i] = {};

      if ('label' in item) {
        if (typeof item.label === 'string' || typeof item.label === 'number' || typeof item.label === 'boolean') {
          filledField.items[i].label = item.label;
        } else {
          errorMessage = 'Unexpected label of item in select: expected string|number|boolean';
          return [null, errorMessage];
        }
      } else {
        errorMessage = 'Unexpected select item without label';
        return [null, errorMessage];
      }

      if ('value' in item) {
        if (typeof item.value === 'string' || typeof item.value === 'number' || typeof item.value === 'boolean') {
          filledField.items[i].value = item.value;
          fieldItemsValues.push(item.value);
        } else {
          errorMessage = 'Unexpected value of item in select: expected string|number|boolean';
          return [null, errorMessage];
        }
      } else {
        errorMessage = 'Unexpected select item without value';
        return [null, errorMessage];
      }
    }

    if ('value' in field) {
      if (filledField.multiple) {
        if (Array.isArray(field.value)) { // TODO: check for multiple array intersection
          filledField.value = field.value;
        } else {
          errorMessage = 'Unexpected value of select: expected array';
          return [null, errorMessage];
        }

        return [filledField, null];
      }

      if (typeof field.value === 'string' || typeof field.value === 'number' || typeof field.value === 'boolean') {
        if (fieldItemsValues.includes(field.value)) {
          filledField.value = field.value;
        } else {
          errorMessage = 'Unexpected value of select: expected value one of item';
          return [null, errorMessage];
        }
      } else {
        errorMessage = 'Unexpected type of select value: expected string|number|boolean';
        return [null, errorMessage];
      }
    }

    return [filledField, null];
  }
  errorMessage = 'Unexpected select field without items: expected array'

  return [null, errorMessage];
}
