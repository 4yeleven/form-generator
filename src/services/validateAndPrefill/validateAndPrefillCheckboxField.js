export default function validateAndPrefill(field) {
  let filledField = {}, errorMessage;

  filledField.type = field.type;
  filledField.label = field.label;
  
  if ('value' in field) {
    if (typeof field.value === 'boolean') {
      filledField.value = field.value;
    } else {
      errorMessage = 'Unexpected value in date field'

      return [null, errorMessage];
    }
  }

  return [filledField, errorMessage];
}
