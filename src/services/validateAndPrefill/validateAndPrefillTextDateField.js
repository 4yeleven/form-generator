const regExp = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;

export default function validateAndPrefill(field) {
  let filledField = {}, errorMessage;

  filledField.type = field.type;
  filledField.label = field.label;
  
  if ('value' in field) {
    if (typeof field.value === 'string') {
      if (regExp.test(field.value)) {
        filledField.value = field.value;
      } else {
        errorMessage = 'Unexpected date format, expected: `yyyy-mm-dd`'
      }
    } else {
      errorMessage = 'Unexpected value in date field'

      return [null, errorMessage];
    }
  }

  if ('placeholder' in field) {
    if (typeof field.placeholder === 'string') {
      filledField.placeholder = field.placeholder;
    } else {
      errorMessage = 'Unexpected placeholder in date field'

      return [null, errorMessage];
    }
  }

  return [filledField, errorMessage];
}
