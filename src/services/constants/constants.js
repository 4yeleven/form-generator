import validateAndPrefillTextField from '../validateAndPrefill/validateAndPrefillTextField';
import validateAndPrefillNumberField from '../validateAndPrefill/validateAndPrefillNumberField';
import validateAndPrefillTextArea from '../validateAndPrefill/validateAndPrefillTextArea';
import validateAndPrefillTextDateField from '../validateAndPrefill/validateAndPrefillTextDateField';
import validateAndPrefillCheckboxField from '../validateAndPrefill/validateAndPrefillCheckboxField';
import validateAndPrefillSelectField from '../validateAndPrefill/validateAndPrefillSelectField';
import validateAndPrefillRadioField from '../validateAndPrefill/validateAndPrefillRadioField';

export const rootContainerId = 'react-root';

export const formFieldTypes = {
  text: 'text',
  number: 'number',
  textarea: 'textarea',
  checkbox: 'checkbox',
  date: 'date',
  radio: 'radio',
  select: 'select',
};

export const validateAndPrefillFieldMap = {
  [formFieldTypes.text]: validateAndPrefillTextField,
  [formFieldTypes.number]: validateAndPrefillNumberField,
  [formFieldTypes.textarea]: validateAndPrefillTextArea,
  [formFieldTypes.checkbox]: validateAndPrefillCheckboxField,
  [formFieldTypes.date]: validateAndPrefillTextDateField,
  [formFieldTypes.radio]: validateAndPrefillRadioField,
  [formFieldTypes.select]: validateAndPrefillSelectField,
};
