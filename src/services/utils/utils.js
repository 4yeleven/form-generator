import { validateAndPrefillFieldMap, formFieldTypes } from '../constants/constants'

const formFieldsList = Object.values(formFieldTypes);

function validateAndPrefillHeader(header) {
  let
    filledHeader = {},
    errorMessage;

  if ('label' in header) {
    if (typeof header.label === 'string' || typeof header.label === 'number') {
      filledHeader.label = header.label;
    } else {
      errorMessage = 'Unexpected label of header: expected string'

      return [null, errorMessage];
    }
  }

  return [filledHeader, null];
}

function validateAndPrefillContent(content) {
  let
    filledContent = {},
    errorMessage;

  if ('items' in content) {
    if (Array.isArray(content.items)) {
      filledContent.items = [];

      for (let i = 0; i < content.items.length; i++) {
        const item = content.items[i];

        if (!formFieldsList.includes(item.type)) {
          errorMessage = `Unexpected type of item: ${item.type}`
          return [null, errorMessage]
        }

        const validateAndPrefillField = validateAndPrefillFieldMap[item.type];
        const [prefilledField, errorMessageF] = validateAndPrefillField(item);

        if (errorMessageF) {
          return [null, errorMessageF]
        }

        filledContent.items.push(prefilledField);
      }

      return [filledContent, null]
    } else {
      errorMessage = 'Unexpected items of content: expected array'
  
      return [null, errorMessage];
    }
  }

  return [filledContent, null];
}

function validateAndPrefillFooter(footer) {
  let
    filledFooter = {},
    errorMessage;

  if ('cancel' in footer) {
    if (typeof footer.cancel.label === 'string' || typeof footer.cancel.label === 'number') {
      filledFooter.cancel = {
        label: footer.cancel.label,
      };
    } else {
      errorMessage = 'Unexpected label of footer cancel button: expected string'

      return [null, errorMessage];
    }
  }

  if ('submit' in footer) {
    if (typeof footer.submit.label === 'string' || typeof footer.submit.label === 'number') {
      filledFooter.submit = {
        label: footer.submit.label,
      };
    } else {
      errorMessage = 'Unexpected label of footer submit button: expected string'

      return [null, errorMessage];
    }
  }

  return [filledFooter, null];
}

export function validateFormStructure(value) {
  try {
    const structure = JSON.parse(value);

    const [header, errorMessageH] = validateAndPrefillHeader(structure.header);

    if (errorMessageH) {
      return [null, errorMessageH];
    }

    const [content, errorMessageC] = validateAndPrefillContent(structure.content);

    if (errorMessageC) {
      return [null, errorMessageC];
    }

    const [footer, errorMessageF] = validateAndPrefillFooter(structure.footer);

    if (errorMessageF) {
      return [null, errorMessageF];
    }

    return [{ header, content, footer }];
  } catch (error) {
    return [null, error.message]
  }
}

export function download(fileName, content) {
  const link = document.createElement('a');
  const file = new Blob([content], { type: 'text/plain' });
  link.href = URL.createObjectURL(file);
  link.download = fileName;
  link.click();
}